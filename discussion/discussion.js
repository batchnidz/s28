// MONGODB Operations
// For creating or inserting data into database

db.users.insert({
        firstName: "Ely",
        lastName: "Buendia",
        age: 50,
        contact: {
                phone: "534776",
                email: "ely@eraserheads.com"
            },
         courses: ["CSS", "Javascript", "Python"],
         department: "none"
    })


//  for querying all the data in database
// Find All
db.users.find()

// Find One
db.users.find({firstName: "Francis", age: 61});

// DeleteOne

db.users.deleteOne({
        firstName: "Ely"
    });

// Update

db.users.updateOne(
    {
        firstName: "Chito"
    },
    {
        $set: {
            lastName: "Esguerra"
        }
    }
);


// Update Many

db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);

// Delete Many

db.users.deleteMany({
        department: "Universal Records"
    });


// Insert Many

db.users.insertMany([
        {
                firstName: "Chito",
                lastName: "Miranda",
                age: 43,
                contact: {
                        phone: "5347786",
                        email: "chito@parokya.com"
                },
                courses: ["Python", "React", "PHP"],
                department: "none"
        },
        {
                firstName: "Francis",
                lastName: "Magalona",
                age: 61,
                contact: {
                        phone: "5347786",
                        email: "francisM@email.com"
                },
                courses: ["React", "Laravel", "SASS"],
                department: "none"
        }
])